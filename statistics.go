package main

import (
	"log"
	"sort"
)

func collectStatusCodeSums(analysis *Analysis) {
	statusStats := make(map[int]int)
	total := 0
	for i := 0; i < len(analysis.FaultyWebsites); i++ {
		code := analysis.FaultyWebsites[i].StatusCode
		codeValue, isKnownCode := statusStats[code]
		if isKnownCode {
			statusStats[code] = codeValue + 1
		} else {
			statusStats[code] = 1
		}
		total = total + 1
	}
	analysis.StatusStats = statusStats
	analysis.TotalFaultyURLs = total
}

func generateStatsFromAnalysis(searchAreaID string, analysis *Analysis, statsFileName string) (Statistics, error) {
	var stats Statistics
	err := readJSON(&stats, statsFileName)
	if err != nil {
		return stats, err
	}
	statsEntry := StatisticsEntry{AnalyzedAt: analysis.AnalysisGenDate.Format("2006-01-02"), StatusCodeSums: analysis.StatusStats, TotalFaultyURLs: analysis.TotalFaultyURLs, TotalURLsAnalyzed: analysis.TotalURLsAnalyzed}
	if stats.AreaAsOsmObjectID == "" {
		// in case we have not had any JSON input as basis
		stats.AreaAsOsmObjectID = searchAreaID
		stats.AreaAsOsmObjectType = analysis.AreaAsOsmObjectType
	} else {
		// JSON could be read and is ready for appending if the today's stats are not yet saved
		isStatsFromTodayAlreadyCaptured := false
		for i := 0; i < len(stats.Entries); i++ {
			if stats.Entries[i].AnalyzedAt == statsEntry.AnalyzedAt {
				isStatsFromTodayAlreadyCaptured = true
			}
		}
		if isStatsFromTodayAlreadyCaptured {
			if *d {
				log.Println("Statistics from today are already added, so not adding them again:", statsEntry)
			}
			return stats, nil
		}
	}
	stats.Entries = append(stats.Entries, statsEntry)
	writeNewJSON(stats, statsFileName)
	if *ddd {
		log.Println("Stats:", stats)
	}
	return stats, nil
}

func collectAllStatusValuesSorted(stats Statistics) []int {
	var statusCodes []int
	for i := 0; i < len(stats.Entries); i++ {
		e := stats.Entries[i]
		for key := range e.StatusCodeSums {
			statusCodes = append(statusCodes, key)
		}
	}
	if *ddd {
		log.Println("all status codes incl. duplicates", statusCodes)
	}

	// unique
	uniqueStatus := make([]int, 0, len(statusCodes))
	m := make(map[int]bool)
	for _, val := range statusCodes {
		if _, ok := m[val]; !ok {
			m[val] = true
			uniqueStatus = append(uniqueStatus, val)
		}
	}
	if *ddd {
		log.Println("unique status:", uniqueStatus)
	}
	// sort
	arrSlice := uniqueStatus[:]
	sort.Ints(arrSlice)
	if *ddd {
		log.Println("sorted status:", uniqueStatus)
	}
	return uniqueStatus
}

func generateChartJsStats(stats Statistics, colorPalette map[int]string) (ChartJsStats, error) {
	allStatus := collectAllStatusValuesSorted(stats)
	if *ddd {
		log.Println("sum of all different status is:", len(allStatus))
	}
	loopCircles := 0
	// initialize data
	var jsData ChartJsStats
	for i := 0; i < len(allStatus); i++ {
		var cjd ChartJsDataset
		cjd.Label = allStatus[i]
		color, exists := colorPalette[allStatus[i]]
		if exists {
			cjd.BackgroundColor = color
			cjd.BorderColor = color
		} else {
			cjd.BackgroundColor = defaultColor
			cjd.BorderColor = defaultColor
		}
		for k := 0; k < len(stats.Entries); k++ {
			cjd.Dataset = append(cjd.Dataset, 0)
			loopCircles = loopCircles + 1
		}
		jsData.DataSets = append(jsData.DataSets, cjd)
	}

	// fill in real data
	// iterate over all days
	for i := 0; i < len(stats.Entries); i++ {
		e := stats.Entries[i]
		jsData.Labels = append(jsData.Labels, e.AnalyzedAt)
		// iterate over map of status from day
		for statusCode, statusCodeSum := range e.StatusCodeSums {
			// iterate over chart.js Datasets to find the corresponding status array
			for k := 0; k < len(jsData.DataSets); k++ {
				loopCircles = loopCircles + 1
				ds := jsData.DataSets[k]
				if statusCode == ds.Label {
					// use i here in order to find the right day in the already initialized dataset
					ds.Dataset[i] = statusCodeSum
				}
			}
		}
		//statusCodeLastVist, isStatusInStats := e.StatusCodeSums[url]
	}
	if *ddd {
		log.Println("chart.js data:", jsData)
	}
	if *dd {
		log.Println("did a a lot of loops:", loopCircles)
	}
	return jsData, nil
}
