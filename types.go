package main

import "time"

type OlvConfiguration struct {
	// Path where the cache files are saved to (overpass and analysis results)
	CachePath string
	// OsmUserName is used for the about page to give a link to your OpenStreetMap user page
	OsmUserName string
	// Path where the analysis website is saved to
	OutputPath string
	// if the output directory is not hosted at the / of your domain, but in a subdirectory, adjust this variable, needs to end with a slash and defaults to a slash / only
	WebOutputSubdir string
}
type FaultyWebsite struct {
	ID         int64
	Type       string
	Lat        float64
	Lon        float64
	Name       string
	Operator   string
	URL        string
	StatusCode int
	Status     string
	FinalURL   string
	Comment    string
	TagType    string
}

type POIwithoutURL struct {
	ID      int64
	Type    string
	Lat     float64
	Lon     float64
	Comment string
	// OSM Tags
	AddrCity        string
	AddrCountry     string
	AddrHousenumber string
	AddrPostcode    string
	AddrStreet      string
	AddrSuburb      string
	Amenity         string
	ContactEmail    string
	Email           string
	Leisure         string
	Name            string
	Operator        string
	Shop            string
	Tourism         string
}

type Analysis struct {
	AreaAsOsmObjectID        int64
	AreaAsOsmObjectType      string
	AreaAsOsmObjectName      string
	AreaAsOsmObjectLatitude  float64
	AreaAsOsmObjectLongitude float64
	AnalysisGenDate          time.Time
	ChangesetComment         string
	ChangesetCommentAddURL   string
	ChangesetHashtags        string
	ChartJsData              ChartJsStats
	ColorStatusCodes         map[int]string
	CustomStatusCodes        map[int]string
	FaultyWebsites           []FaultyWebsite
	GeneratingPerson         string
	MaxCharsForUrlName       int
	PageGenDate              time.Time
	POIsNoURL                []POIwithoutURL
	StatusStats              map[int]int
	TemplateName             string
	TimeForOverpass          time.Duration
	TimeForOverpassPOIsNoURL time.Duration
	TimeForAnalysis          time.Duration
	Title                    string
	TotalCommonRedirects     int
	TotalFaultyURLs          int
	TotalIgnoredURLs         int
	TotalOsmObjectsAnalyzed  int
	TotalPOIsMissingURL      int
	TotalURLsAnalyzed        int
	Version                  string
	IgnoreListOfURLs         IgnoreList
	WebOutputSubdir          string
}

type ChartJsStats struct {
	Labels   []string
	DataSets []ChartJsDataset
}

type ChartJsDataset struct {
	BackgroundColor string
	BorderColor     string
	Dataset         []int
	Label           int
}

type Statistics struct {
	AreaAsOsmObjectID   string
	AreaAsOsmObjectType string
	Entries             []StatisticsEntry
}

type StatisticsEntry struct {
	AnalyzedAt        string
	StatusCodeSums    map[int]int
	TotalFaultyURLs   int
	TotalURLsAnalyzed int
}

type IgnoreList struct {
	Entries []IgnoreListEntry
}

type IgnoreListEntry struct {
	Status   int
	StartURL string
	FinalURL string
}
