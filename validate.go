package main

import (
	"errors"
	"io"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
)

const maxRedirects = 10
const ignoreListFilename = "ignorelist.json"
const commonsWikiURL = "https://commons.wikimedia.org/wiki/"
const constPrefixHttps = "https://"
const constPrefixHttpsWww = "https://www."
const constPrefixHttp = "http://"
const constPrefxHttpWww = "http://www."
const httpErrorUnsupportedProtocol = "unsupported protocol scheme"
const httpErrorVerifyCertificateX509Prefix = "tls: failed to verify certificate: x509"
const httpErrorVerifyCertificateX509UnkownAuthority = "certificate signed by unknown authority"
const httpErrorVerifyCertificateX509CertExpired = "certificate has expired or is not yet valid"
const httpErrorVerifyCertificateX509CertOtherDomain = "certificate is valid for"
const filePrefixURL = "File:"
const websiteHostedBy = "website hosted by "
const osmURL = "https://osm.org/"

func generateStatusText(code int, status string, text string) string {
	if text == "" {
		return strconv.Itoa(code) + " " + status
	}
	return strconv.Itoa(code) + " " + status + ": " + text
}

func isCDN(h http.Header) (bool, string) {
	returnString := websiteHostedBy
	if strings.ToLower(h.Get("Server")) == "cloudflare" {
		return true, returnString + "Cloudflare"
	}
	if strings.ToLower(h.Get("Server")) == "cloudfront" {
		return true, returnString + "CloudFront"
	}
	if strings.ToLower(h.Get("Server")) == "datadome" {
		return true, returnString + "DataDome"
	}
	if strings.ToLower(h.Get("Server")) == "akamai" {
		return true, returnString + "Akamai (server)"
	}
	if strings.ToLower(h.Get("Server")) == "akamaighost" {
		return true, returnString + "AkamaiGHost"
	}
	for k := range h {
		if strings.Contains(strings.ToLower(k), "akamai") {
			return true, returnString + "Akamai (key)"
		}
	}
	return false, returnString
}

func isDomainParking(htmlBody string) (bool, string) {
	htmlBodyLower := strings.ToLower(htmlBody)
	stringsIdentifyingDomainParking := [...]string{"https://cdn.sedo.com", "www.denic.de", "https://www.odoo.com", "domainmarkt.de", "https://www.hugedomains.com", "https://www.1und1.de", "webpackJsonpparking-lander", "parkingcrew.net", "sedoparking.com", "parking.bodiscdn.com", "anymondo.com", "elitedomains.de", "wsimg.com/parking-lander", "nicsell.com", "domainname.de", "domaincatcher.com", "kv-gmbh.de", "selling.fizon.de", "mydomaincontact.com"}
	for i := 0; i < len(stringsIdentifyingDomainParking); i++ {
		if strings.Contains(htmlBodyLower, stringsIdentifyingDomainParking[i]) {
			return true, stringsIdentifyingDomainParking[i]
		}
	}
	return false, ""
}

func getURL(theURL string, client http.Client, customHttpStatus map[int]string) (statusNumber int, statusString string, finalURL string) {
	status := ""
	statusCode := -1
	nextURL := theURL
	var dnsErr *net.DNSError
	var urlErr *url.Error
	for i := 0; i < maxRedirects; i++ {
		req, err := http.NewRequest("GET", nextURL, nil)
		if err != nil {
			statusCode = 901
			return statusCode, strconv.Itoa(statusCode) + " " + customHttpStatus[statusCode] + err.Error(), ""
		}
		req.Header.Set("User-Agent", defaultUserAgentForBrowsing)
		resp, err := client.Do(req)
		if err != nil {
			if os.IsTimeout(err) {
				statusCode = 912
				return statusCode, generateStatusText(statusCode, customHttpStatus[statusCode], err.Error()), ""
			} else if errors.As(err, &dnsErr) {
				statusCode = 913
				return statusCode, generateStatusText(statusCode, customHttpStatus[statusCode], err.Error()), ""
			} else if errors.As(err, &urlErr) && strings.HasPrefix(urlErr.Err.Error(), httpErrorUnsupportedProtocol) {
				statusCode = 914
				statusText := err.Error()
				finalURL = ""
				if strings.HasPrefix(nextURL, filePrefixURL) {
					tryCommonsImageURL := commonsWikiURL + strings.ReplaceAll(nextURL, " ", "_")
					tryStatusNr, tryStatusText, tryFinalURL := getURL(tryCommonsImageURL, client, customHttpStatus)
					statusCode = tryStatusNr
					if tryStatusNr != 200 {
						statusText = statusText + ". The commons image URL " + tryCommonsImageURL + " we'd tried did not respond with OK: " + tryStatusText
						finalURL = tryFinalURL
					}
				} else {
					statusText = statusText + ". Maybe the URL <a href=\"" + constPrefixHttps + nextURL + "\">" + constPrefixHttps + nextURL + "</a> or <a href=\"" + constPrefixHttp + nextURL + "\">" + constPrefixHttp + nextURL + "</a> works."
				}
				return statusCode, generateStatusText(statusCode, customHttpStatus[statusCode], statusText), finalURL
			} else if errors.As(err, &urlErr) && strings.HasPrefix(urlErr.Err.Error(), httpErrorVerifyCertificateX509Prefix+": "+httpErrorVerifyCertificateX509UnkownAuthority) {
				statusCode = 915
				return statusCode, generateStatusText(statusCode, customHttpStatus[statusCode], err.Error()), ""
			} else if errors.As(err, &urlErr) && strings.HasPrefix(urlErr.Err.Error(), httpErrorVerifyCertificateX509Prefix+": "+httpErrorVerifyCertificateX509CertExpired) {
				statusCode = 916
				return statusCode, generateStatusText(statusCode, customHttpStatus[statusCode], err.Error()), ""
			} else if errors.As(err, &urlErr) && strings.HasPrefix(urlErr.Err.Error(), httpErrorVerifyCertificateX509Prefix+": "+httpErrorVerifyCertificateX509CertOtherDomain) {
				statusCode = 917
				return statusCode, generateStatusText(statusCode, customHttpStatus[statusCode], err.Error()), ""
			} else {
				statusCode = 911
				return statusCode, generateStatusText(statusCode, customHttpStatus[statusCode], err.Error()), ""
			}
		}
		defer resp.Body.Close()
		// save first status code before all redirects
		if i == 0 {
			statusCode = resp.StatusCode
			status = resp.Status
		}
		if resp.StatusCode < 300 || resp.StatusCode >= 400 {
			// if last status code, which is not a redirect, is not 200, save this current status as return status
			if resp.StatusCode != 200 {
				statusCode = resp.StatusCode
				status = resp.Status
				// check for response header if a CDN is blocking access
				isCDNused, text := isCDN(resp.Header)
				if isCDNused {
					statusCode = 918
					return statusCode, generateStatusText(statusCode, customHttpStatus[statusCode], text), ""
				}
			}
			if *ddd {
				log.Println("let's look into the body of the URL " + nextURL + " (redirected from " + theURL + ")")
			}
			b, err := io.ReadAll(resp.Body)
			content := string(b)
			if err != nil {
				if *d {
					log.Println("error reading body content of the URL " + nextURL + " (redirected from " + theURL + "): " + err.Error())
				}
			}
			// check if domain is available for buying
			isDomainParked, whichParking := isDomainParking(content)
			if isDomainParked {
				statusCode = 922
				statusText := "found string " + whichParking + " in it"
				return statusCode, generateStatusText(statusCode, customHttpStatus[statusCode], statusText), ""
			}
			break // for loop of the max redirects
		} else {
			// use resp.Location() instead of resp.Header.Get("Location") here because the latter can contain relative paths
			locationURL, err := resp.Location()
			if err != nil {
				if *d {
					log.Println("original url was " + theURL + " and now for " + nextURL + ": " + err.Error())
				}
				break
			}
			nextURL = locationURL.String()
		}
	}
	return statusCode, status, nextURL
}

func removeTrailingSlash(s string) string {
	if strings.HasSuffix(s, "/") {
		return s[:len(s)-1]
	}
	return s
}

func isWWWRedirect(startURL, finalURL string) bool {
	// remove any suffix slash to make URLs comparable
	cmpStartURL := removeTrailingSlash(startURL)
	cmpFinalURL := removeTrailingSlash(finalURL)
	if strings.HasPrefix(cmpStartURL, constPrefixHttpsWww) && strings.HasPrefix(cmpFinalURL, constPrefixHttps) && strings.ReplaceAll(cmpFinalURL, constPrefixHttps, constPrefixHttpsWww) == cmpStartURL {
		return true
	} else if strings.HasPrefix(cmpStartURL, constPrefixHttps) && strings.HasPrefix(cmpFinalURL, constPrefixHttpsWww) && strings.ReplaceAll(cmpFinalURL, constPrefixHttpsWww, constPrefixHttps) == cmpStartURL {
		return true
	} else if strings.HasPrefix(cmpStartURL, constPrefxHttpWww) && strings.HasPrefix(cmpFinalURL, constPrefixHttp) && strings.ReplaceAll(cmpFinalURL, constPrefixHttp, constPrefxHttpWww) == cmpStartURL {
		return true
	} else if strings.HasPrefix(cmpStartURL, constPrefixHttp) && strings.HasPrefix(cmpFinalURL, constPrefxHttpWww) && strings.ReplaceAll(cmpFinalURL, constPrefxHttpWww, constPrefixHttp) == cmpStartURL {
		return true
	}
	return false
}

func checkForCommonRedirect(startURL, finalURL string) bool {
	originalURL := strings.ToLower(startURL)
	redirectURL := strings.ToLower(finalURL)
	// if redirected URL is the same as the original URL, this is not a common redirect
	if originalURL == redirectURL {
		return false
	}
	if isWWWRedirect(originalURL, redirectURL) {
		return true
	}
	// same URL but with an ending slash
	if redirectURL == originalURL+"/" {
		return true
	}
	// same URL but removed the ending slash
	if strings.HasSuffix(originalURL, "/") && redirectURL == originalURL[:len(originalURL)-1] {
		return true
	}
	// detect common redirects, commonRedirects shall only contain lowercase strings
	commonRedirects := [...]string{"de", "de-de", "de_de", "de/de", "?lang=de", "eu-de", "en", "en-en", "en-de", "en/de", "en-us", "en-gb", "en-eu", "it", "pl", "de.html", "en.html", "index", "index.php", "index.htm", "index.html", "index2.html", "index.php/de", "index.php/en", "index.aspx", "startseite", "site/startseite", "startseite.html", "de/startseite", "de/start.html", "de/angebote", "home", "de/home", "en/home", "de/de/home", "de/home.html", "home.html", "home.php", "de/index", "de/index.php", "de/index.html", "en/index.html", "en/index", "en/index.php", "start", "start.htm", "start.html", "de/homepage.html", "homepage.html", "homepage", "de-DE/home.page", "shop", "willkommen", "willkommen.html", "de/willkommen.html", "welcome", "welcome.html", "privatkunden", "wordpress", "content", "offline", "aktuell", "aktuelles", "cms", "news", "news.php", "ru", "web", "index.php/Startseite", "apotheke/willkommen.htm", "index.php?id=2", "password", "offline", "maintenance", "startseite.cfm", "intro", "wp", "Wordpress", "1", "bc", "delivery", "maintenance.html", "hauptseite.html"}
	for i := 0; i < len(commonRedirects); i++ {
		if strings.HasSuffix(originalURL, "/") {
			if redirectURL == originalURL+commonRedirects[i] || redirectURL == originalURL+commonRedirects[i]+"/" {
				return true
			}
		} else {
			if redirectURL == originalURL+commonRedirects[i] || redirectURL == originalURL+"/"+commonRedirects[i] || redirectURL == originalURL+"/"+commonRedirects[i]+"/" {
				return true
			}
		}
	}
	return false
}

func browseToOsmURL(startURL string, e Element, c *http.Client, sumOfURLs *int, customHttpStatus map[int]string, sumCommonRedirects *int) (returnStatusCode int, returnStatus string, returnFinalURL string) {
	returnStatusCode = -1
	if startURL != "" {
		statusCode, status, finalURL := getURL(startURL, *c, customHttpStatus)
		if *ddd {
			log.Println("URL visited:", statusCode, status, startURL, finalURL, e.ID, e.Tags.Name, e.Tags.Operator)
		}
		returnStatusCode = statusCode
		returnStatus = status
		returnFinalURL = finalURL
		*sumOfURLs += 1
	} else {
		return returnStatusCode, returnStatus, returnFinalURL
	}
	if startURL == returnFinalURL {
		returnFinalURL = ""
	}
	if checkForCommonRedirect(startURL, returnFinalURL) {
		// URL is a common redirection, do not report it
		if *ddd {
			log.Println("found a common redirect from", startURL, "to", returnFinalURL, "and thus not reporting this OSM object", e)
		}
		*sumCommonRedirects = *sumCommonRedirects + 1
		return -1, "", ""
	} else {
		if *ddd {
			log.Println("not a common redirect from", startURL, "to", returnFinalURL, " for this OSM object", e)
		}
	}
	return returnStatusCode, returnStatus, returnFinalURL
}

func addFaultWebsite(a *Analysis, e Element, tagType string, url string, statusCode int, status string, finalURL string, comment string) {
	if statusCode >= 0 && statusCode != 200 {
		lat := e.Lat
		lon := e.Lon
		if e.Type == "way" || e.Type == "relation" {
			lat = e.Center.Lat
			lon = e.Center.Lon
		}
		fw := FaultyWebsite{ID: e.ID, Type: e.Type, Lat: lat, Lon: lon, Name: e.Tags.Name, Operator: e.Tags.Operator, URL: url, StatusCode: statusCode, Status: status, FinalURL: finalURL, Comment: comment, TagType: tagType}
		a.FaultyWebsites = append(a.FaultyWebsites, fw)
	}
}

func checkForUrlShorteners(theURL string) (bool, string) {
	listOfUrlShorteners := [...]string{"alturl.com", "bit.ly", "cutt.ly", "goo.gl", "g.co", "ow.ly", "shorturl.at", "shorturl.gg", "t.co", "tiny.cc", "tiny.pl", "tinyurl.com", "w.wiki", "xing.to", "youtu.be", "ttm.sh", "is.gd", "0x0.st", "shorta.link"}
	foundUrlShortener := ""
	testURL := strings.ToLower(theURL)
	for i := 0; i < len(listOfUrlShorteners); i++ {
		if strings.Contains(testURL, listOfUrlShorteners[i]+"/") {
			foundUrlShortener = listOfUrlShorteners[i]
			return true, foundUrlShortener
		}
	}
	return false, foundUrlShortener
}

func checkForTrackingParameter(theURL string) (bool, string) {
	listOfTrackingParameters := [...]string{"mc_id", "utm_source", "utm_medium", "utm_term", "utm_content", "utm_campaign", "fbclid", "gclid", "campaign_ref", "gclsrc", "dclid", "wt.tsrc", "zanpid", "yclid", "igshid"}
	foundTrackingParameter := ""
	testURL := strings.ToLower(theURL)
	for i := 0; i < len(listOfTrackingParameters); i++ {
		if strings.Contains(testURL, listOfTrackingParameters[i]+"=") {
			foundTrackingParameter = listOfTrackingParameters[i]
			return true, foundTrackingParameter
		}
	}
	return false, foundTrackingParameter
}

func checkForSocialMedia(theURL string) (bool, string) {
	var socialMediaSites = map[string]string{
		"contact:facebook":    "facebook.com",
		"contact:vk":          "vk.com",
		"contact:instagram":   "instagram.com",
		"contact:twitter":     "twitter.com",
		"contact:youtube":     "youtube.com",
		"contact:ok":          "ok.ru",
		"contact:telegram":    "//t.me",
		"contact:linkedin":    "linkedin.com",
		"contact:pinterest":   "pinterest.",
		"contact:foursquare":  "foursquare.com",
		"contact:xing":        "xing.com",
		"contact:flickr":      "flickr.com",
		"contact:tripadvisor": "tripadvisor.com",
	}
	foundSocialMedia := ""
	testURL := strings.ToLower(theURL)
	for osmKey, identifier := range socialMediaSites {
		if strings.Contains(testURL, identifier+"/") {
			foundSocialMedia = osmKey
			return true, foundSocialMedia
		}
	}
	return false, foundSocialMedia
}

// returns true if entry is known in ignore list
func checkForIgnoreList(statusCode int, startURL string, finalURL string, ignList *IgnoreList) bool {
	if *ddd {
		log.Println("checkForIgnoreList:", statusCode, startURL, finalURL)
	}
	// WithOut Slash
	wosStartURL := removeTrailingSlash(startURL)
	wosFinalURL := removeTrailingSlash(finalURL)
	for i := 0; i < len(ignList.Entries); i++ {
		we := ignList.Entries[i]
		weWosStartURL := removeTrailingSlash(we.StartURL)
		weWosFinalURL := removeTrailingSlash(we.FinalURL)
		// Assumption is that weWosStartURL is only the domain and it is the prefix of wosStartURL.
		// In case of status 918 CDN, usually the whole domain uses a CDN, so ignore all subpages of that domain.
		// Also ignore 302 Found for a whole domain, esp. when finalURL is empty like in motel-one.com or leading
		// to defect redirect which will hopefully get fixed some day like in mcdonalds.com.
		// also ignore whole domains where status is 429 Too Many Requests
		if (statusCode == 918 || statusCode == 429 || statusCode == 302) && statusCode == we.Status && strings.HasPrefix(wosStartURL, weWosStartURL) {
			return true
		}
		if statusCode == we.Status && wosStartURL == weWosStartURL && wosFinalURL == weWosFinalURL {
			// generic check for exact match with the ignore list entry
			return true
		} else if statusCode == we.Status && wosStartURL == weWosStartURL && we.FinalURL == "" {
			// When ignore list entry final URL is empty, but start url and status code match, then it is a match.
			// This is useful for URLs which redirect from example.com to example.com/SID=asdf, where the SID is dynamic.
			return true
		}
	}
	return false
}

func analyzeOsmURL(analysis *Analysis, comment *string, e Element, c *http.Client, sumOfURLs *int, theWebsiteTagContent string, websiteTagType string) {
	multipleURLsInOneTag := strings.Split(theWebsiteTagContent, ";")
	if len(multipleURLsInOneTag) > 1 && *dd {
		objectURL := osmURL + e.Type + "/" + strconv.FormatInt(e.ID, 10)
		log.Println("for", objectURL, "there is more than one URL in the tag", websiteTagType, "after splitting by semi-colon:", multipleURLsInOneTag)
	}

	for i := 0; i < len(multipleURLsInOneTag); i++ {
		theURL := strings.TrimSpace(multipleURLsInOneTag[i])
		statusCode, status, finalURL := browseToOsmURL(theURL, e, c, sumOfURLs, analysis.CustomStatusCodes, &analysis.TotalCommonRedirects)
		// check if the POI feature is disused
		if e.Tags.DisusedAmenity != "" || e.Tags.DisusedShop != "" || e.Tags.DisusedTourism != "" {
			*comment = *comment + "POI has a <code>disused:*</code> main feature<br />\n"
			statusCode = 924
			status = generateStatusText(statusCode, analysis.CustomStatusCodes[statusCode], "")
		}
		// check if the URL is a known URL shortener like e.g. bit.ly
		isUrlShortener, urlShortener := checkForUrlShorteners(theURL)
		if isUrlShortener {
			*comment = *comment + "URL shortener " + urlShortener + " used, status of redirect URL is " + status + "<br />\n"
			statusCode = 919
			status = generateStatusText(statusCode, analysis.CustomStatusCodes[statusCode], "")
		}
		// check for typical tracking parameters in the URL
		isTracking, trackingParam := checkForTrackingParameter(theURL)
		if isTracking {
			*comment = *comment + "Remove " + trackingParam + " and maybe more tracking parameter from the URL. URL status is " + status + "<br />\n"
			statusCode = 920
			status = generateStatusText(statusCode, analysis.CustomStatusCodes[statusCode], "")
		}
		// check if the URL is a known social media link, which has a more specialized tag
		// image tag is ignored here, because image hot links to social media sites are ok
		if websiteTagType != "image" {
			isSocialMediaOriginalUrl, socialMediaOsmKeyOriginalUrl := checkForSocialMedia(theURL)
			isSocialMediaRedirectUrl, socialMediaOsmKeyRedirectUrl := checkForSocialMedia(finalURL)
			if isSocialMediaOriginalUrl || isSocialMediaRedirectUrl {
				socialMediaOsmKey := ""
				if socialMediaOsmKeyOriginalUrl == "" {
					socialMediaOsmKey = socialMediaOsmKeyRedirectUrl
				} else {
					socialMediaOsmKey = socialMediaOsmKeyOriginalUrl
				}
				*comment = *comment + "Better use <code>" + socialMediaOsmKey + "</code> instead of <code>" + websiteTagType + "</code>. URL status is " + status + "<br />\n"
				statusCode = 921
				status = generateStatusText(statusCode, analysis.CustomStatusCodes[statusCode], "")
			}
		}
		// check for https version of the website if it reports status 200
		if statusCode == 200 && strings.HasPrefix(theURL, constPrefixHttp) {
			if *ddd {
				log.Println("checking for https version of", theURL)
			}
			httpsURL := strings.ReplaceAll(theURL, constPrefixHttp, constPrefixHttps)
			httpsStatusCode, _, _ := browseToOsmURL(httpsURL, e, c, sumOfURLs, analysis.CustomStatusCodes, &analysis.TotalCommonRedirects)
			if httpsStatusCode == 200 {
				if *ddd {
					log.Println("found a URL supporting TLS:", httpsURL)
				}
				*comment = *comment + "TLS secured version of the website is available: <a href='" + httpsURL + "'>" + httpsURL + "</a><br />\n"
				statusCode = 923
				status = generateStatusText(statusCode, analysis.CustomStatusCodes[statusCode], "")
			}
		}
		// checkForIgnoreList is called after the check for custom status, because otherwise false positves cannot be ignored then
		if checkForIgnoreList(statusCode, theURL, finalURL, &analysis.IgnoreListOfURLs) {
			if *ddd {
				log.Println("Not reporting ignore-listed URL", theURL)
			}
			analysis.TotalIgnoredURLs = analysis.TotalIgnoredURLs + 1
			return
		}
		if statusCode >= 0 {
			addFaultWebsite(analysis, e, websiteTagType, theURL, statusCode, status, finalURL, *comment)
		} else {
			if *ddd {
				// usually common redirects, which report back a status code of -1
				log.Println("not reporting this URL with status", statusCode, status, "for", theURL, "in object", e, "and redirect URL is", finalURL)
			}
		}
	}
}

func getNumberOfDifferentWebsiteTagsOfOsmObject(e Element) (int, string) {
	nr := 0
	usedTags := ""
	if e.Tags.Website != "" {
		nr = nr + 1
		usedTags = usedTags + "website "
	}
	if e.Tags.ContactWebsite != "" {
		nr = nr + 1
		usedTags = usedTags + "contact:website "
	}
	if e.Tags.URL != "" {
		nr = nr + 1
		usedTags = usedTags + "url "
	}
	return nr, usedTags
}

func analyzeOsmObject(analysis *Analysis, e Element, c *http.Client, sumOfURLs *int) {
	comment := ""
	// check if multiple websites are tagged with different keys
	numberOfDifferentWebsiteTags, websiteTagsUsed := getNumberOfDifferentWebsiteTagsOfOsmObject(e)
	if numberOfDifferentWebsiteTags == 0 {
		if *ddd {
			log.Println("OSM object has no main website tags (website, contact:website or url):", e)
		}
	} else if numberOfDifferentWebsiteTags > 1 {
		comment = comment + "Object uses the following " + strconv.Itoa(numberOfDifferentWebsiteTags) + " different tags for URLs: " + websiteTagsUsed + "<br/>\n"
	}
	if e.Tags.Website != "" {
		analyzeOsmURL(analysis, &comment, e, c, sumOfURLs, e.Tags.Website, "website")
	}
	if e.Tags.ContactWebsite != "" {
		analyzeOsmURL(analysis, &comment, e, c, sumOfURLs, e.Tags.ContactWebsite, "contact:website")
	}
	if e.Tags.URL != "" {
		analyzeOsmURL(analysis, &comment, e, c, sumOfURLs, e.Tags.URL, "url")
	}
	if e.Tags.Image != "" {
		analyzeOsmURL(analysis, &comment, e, c, sumOfURLs, e.Tags.Image, "image")
	}
	if e.Tags.HeritageWebsite != "" {
		analyzeOsmURL(analysis, &comment, e, c, sumOfURLs, e.Tags.HeritageWebsite, "heritage:website")
	}
	if e.Tags.BrandWebsite != "" {
		analyzeOsmURL(analysis, &comment, e, c, sumOfURLs, e.Tags.BrandWebsite, "brand:website")
	}
	if e.Tags.OperatorWebsite != "" {
		analyzeOsmURL(analysis, &comment, e, c, sumOfURLs, e.Tags.OperatorWebsite, "operator:website")
	}
	if e.Tags.MemorialWebsite != "" {
		analyzeOsmURL(analysis, &comment, e, c, sumOfURLs, e.Tags.MemorialWebsite, "memorial:website")
	}
	if e.Tags.NetworkWebsite != "" {
		analyzeOsmURL(analysis, &comment, e, c, sumOfURLs, e.Tags.NetworkWebsite, "network:website")
	}
	if e.Tags.PaymentWebsite != "" {
		analyzeOsmURL(analysis, &comment, e, c, sumOfURLs, e.Tags.PaymentWebsite, "payment:website")
	}
	if e.Tags.WebsiteMenu != "" {
		analyzeOsmURL(analysis, &comment, e, c, sumOfURLs, e.Tags.WebsiteMenu, "website:menu")
	}
	if e.Tags.WebsiteStock != "" {
		analyzeOsmURL(analysis, &comment, e, c, sumOfURLs, e.Tags.WebsiteStock, "website:stock")
	}
	if e.Tags.WebsiteMap != "" {
		analyzeOsmURL(analysis, &comment, e, c, sumOfURLs, e.Tags.WebsiteMap, "website:map")
	}
	if e.Tags.WebsiteBooking != "" {
		analyzeOsmURL(analysis, &comment, e, c, sumOfURLs, e.Tags.WebsiteBooking, "website:booking")
	}
	if e.Tags.SourceWebsite != "" {
		analyzeOsmURL(analysis, &comment, e, c, sumOfURLs, e.Tags.SourceWebsite, "source:website")
	}
	if e.Tags.WebsiteSource != "" {
		analyzeOsmURL(analysis, &comment, e, c, sumOfURLs, e.Tags.WebsiteSource, "website:source")
	}
	if e.Tags.XmasURL != "" {
		analyzeOsmURL(analysis, &comment, e, c, sumOfURLs, e.Tags.WebsiteSource, "xmas:url")
	}
}

func osmObjectWithoutURL(analysis *Analysis, e Element) {
	var o POIwithoutURL
	o.ID = e.ID
	o.Type = e.Type
	o.Lat = e.Lat
	o.Lon = e.Lon
	if o.Type == "way" || o.Type == "relation" {
		o.Lat = e.Center.Lat
		o.Lon = e.Center.Lon
	}
	// OSM Tags
	o.AddrCity = e.Tags.AddrCity
	o.AddrCountry = e.Tags.AddrCountry
	o.AddrHousenumber = e.Tags.AddrHousenumber
	o.AddrPostcode = e.Tags.AddrPostcode
	o.AddrStreet = e.Tags.AddrStreet
	o.AddrSuburb = e.Tags.AddrSuburb
	o.Amenity = e.Tags.Amenity
	o.ContactEmail = e.Tags.ContactEmail
	o.Email = e.Tags.ContactEmail
	o.Leisure = e.Tags.Leisure
	o.Name = e.Tags.Name
	o.Operator = e.Tags.Operator
	o.Shop = e.Tags.Shop
	o.Tourism = e.Tags.Tourism
	analysis.POIsNoURL = append(analysis.POIsNoURL, o)
}
