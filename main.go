package main

import (
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"
)

var (
	version = "dev"
	commit  = "none"
	date    = "unknown"
	builtBy = "unknown"
)

const defaultUserAgentForBrowsing = "Mozilla/5.0 (X11; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0"
const defaultAnalysisMaxAgeInHours = 16
const defaultOverpassDataMaxAgeInHours = 16
const defaultOverpassNameResultMaxAgeInHours = 24 * 30
const defaultColor = "#0248f9"
const cssDir = "css"
const imagesDir = "images"
const maxCharsForUrlName = 100
const defaultChangesetComment = "fix%20URL"
const defaultChangesetCommentAddURL = "add%20URL"
const defaultChangesetHashtags = "#strubblOLV"
const timeOutAnalyzingOneURL = 20

func main() {
	start := time.Now()
	log.SetOutput(os.Stdout)
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	handleFlags()
	if *d {
		log.Printf("osm-link-validator version %s, commit %s, built at %s by %s", version, commit, date, builtBy)
	}
	// read config values
	var olvConfig OlvConfiguration
	err := readWriteConfigFile(*givenConfig, &olvConfig)
	if err != nil {
		log.Panicln("could not read the config file at", *givenConfig, "and got an error:", err)
	}

	// thanks to https://flatuicolors.com/palette/de and https://flatuicolors.com/palette/defo
	var statusColors = map[int]string{
		301: "#fc5c65", // start palette/de
		302: "#fd9644",
		303: "#fed330",
		307: "#26de81",
		308: "#2bcbba",
		400: "#eb3b5a",
		401: "#fa8231",
		403: "#f7b731",
		404: "#20bf6b",
		410: "#0fb9b1",
		429: "#45aaf2",
		500: "#4b7bec",
		502: "#a55eea",
		503: "#d1d8e0",
		530: "#778ca3",
		901: "#2d98da",
		911: "#3867d6",
		912: "#8854d0",
		913: "#a5b1c2",
		914: "#4b6584", // end palette/de
		915: "#1abc9c", // start palette/defo
		916: "#2ecc71",
		917: "#3498db",
		918: "#9b59b6",
		919: "#34495e",
		920: "#16a085",
		921: "#27ae60",
		922: "#2980b9",
		923: "#8e44ad",
		924: "#2c3e50", // further colors: #f1c40f #e67e22 #e74c3c #ecf0f1 #95a5a6 #f39c12 #d35400 #c0392b #bdc3c7 #7f8c8d
	}
	// custom status codes, which are not directly coupled to HTTP status codes
	var customStatusCodes = map[int]string{
		901: "Error creating HTTP request",
		911: "Error doing request",
		912: "Timeout doing request",
		913: "DNS Error doing request",
		914: "Protocol Error doing request",
		915: "Certificate authority",
		916: "Certificate expired",
		917: "Certificate domain",
		918: "Unable to analyze URL because a CDN is used",
		919: "URL shortener used",
		920: "Tracking parameter found",
		921: "Social Media URL detected",
		922: "Domain is for sale",
		923: "TLS version available",
		924: "POI without feature",
	}

	var ignoreList IgnoreList
	err = readJSON(&ignoreList, ignoreListFilename)
	if err != nil {
		log.Println(err)
	}
	if *cleanIgnoreListFlag && err != nil {
		os.Exit(1)
	}
	if *cleanIgnoreListFlag {
		var updatedIgnoreList IgnoreList
		cleanClient := http.Client{
			Timeout: timeOutAnalyzingOneURL * time.Second,
			CheckRedirect: func(req *http.Request, via []*http.Request) error {
				return http.ErrUseLastResponse
			},
		}
		var nrItemsRemoved int
		for i := 0; i < len(ignoreList.Entries); i++ {
			ile := ignoreList.Entries[i]
			if ile.Status == 918 {
				// always ignore status 918 CDN used because if it only works this time, next time it might fail again
				updatedIgnoreList.Entries = append(updatedIgnoreList.Entries, ile)
				continue
			}
			startURL := ile.StartURL
			statusCode, _, finalURL := getURL(startURL, cleanClient, customStatusCodes)
			isUrlShortener, _ := checkForUrlShorteners(startURL)
			if isUrlShortener {
				statusCode = 919
			}
			isTracking, _ := checkForTrackingParameter(startURL)
			if isTracking {
				statusCode = 920
			}
			isSocialMediaOriginalUrl, _ := checkForSocialMedia(startURL)
			isSocialMediaRedirectUrl, _ := checkForSocialMedia(finalURL)
			if isSocialMediaOriginalUrl || isSocialMediaRedirectUrl {
				statusCode = 921
			}
			if ile.FinalURL == "" {
				finalURL = ""
			}
			if finalURL == ile.FinalURL && statusCode == ile.Status {
				if *ddd {
					log.Println("keeping ignore list item:", ile)
				}
				updatedIgnoreList.Entries = append(updatedIgnoreList.Entries, ile)
			} else {
				if *d {
					nrItemsRemoved++
					log.Println("removing ignore list item:", ile, "cause status code", statusCode, "or final url", finalURL, "is different")
				}
			}
		}
		log.Println("removed", nrItemsRemoved, "of", len(ignoreList.Entries), "items from the ignore list")
		err = WriteNonEscapedJSON(updatedIgnoreList, ignoreListFilename)
		if err != nil {
			log.Fatalln(err)
		}
		os.Exit(0)
	}

	// get fresh data either from disk or overpass-api
	initCacheAndOutputDir(olvConfig.CachePath, olvConfig.OutputPath)
	defaultOverpassResultFileName := olvConfig.CachePath + string(os.PathSeparator) + defaultOverpassResultFileNamePrefix + searchAreaID + defaultOverpassResultFileNamePostfix
	defaultOverpassURL := defaultOverpassAreaPrefix + searchAreaID + defaultOverpassURLpostfix
	if *dd {
		log.Println("used overpass API call", defaultOverpassURL)
	}
	var timeForOverpass time.Duration
	isOverpassResultOutdated := isDataOutdated(defaultOverpassResultFileName, defaultOverpassDataMaxAgeInHours)
	if isOverpassResultOutdated {
		timeForOverpassStart := time.Now()
		downloadOverpassJSON(defaultOverpassURL, defaultOverpassResultFileName)
		timeForOverpass = time.Since(timeForOverpassStart)
	}
	// read overpass-api json result
	var overpassData OverpassResult
	err = readJSON(&overpassData, defaultOverpassResultFileName)
	if err != nil {
		log.Fatalln(err)
	}
	if len(overpassData.Elements) == 0 {
		log.Fatalln("Overpass returned a result with no OSM objects. Investigate or delete file", defaultOverpassResultFileName)
	}

	analysisFileName := olvConfig.CachePath + string(os.PathSeparator) + "analysis-" + searchAreaID + ".json"
	isAnalysisOutdated := isDataOutdated(analysisFileName, defaultAnalysisMaxAgeInHours)
	var analysis Analysis
	searchAreaIDint64, err := strconv.ParseInt(searchAreaID, 10, 64)
	if err != nil {
		log.Printf("cannot convert searchAreaID=%s to int: %v\n", searchAreaID, err)
	}
	objectID, objectType := getOsmObjectIdFromAreaId(searchAreaIDint64)
	if *dd {
		log.Println("using found OSM object ID", objectID, "and type", objectType, "as information in the analysis")
	}
	if objectID != searchAreaIDint64 {
		analysis.AreaAsOsmObjectID = objectID
		analysis.AreaAsOsmObjectType = objectType

		overpassNameURL := "https://overpass-api.de/api/interpreter?data=[out:json][timeout:25];(" + objectType + "(" + strconv.FormatInt(objectID, 10) + "););out%20center;"
		overpassNameResultFileName := olvConfig.CachePath + string(os.PathSeparator) + defaultOverpassResultFileNamePrefix + "name-" + searchAreaID + defaultOverpassResultFileNamePostfix
		isOverpassNameOutdated := isDataOutdated(overpassNameResultFileName, defaultOverpassNameResultMaxAgeInHours)
		if isOverpassNameOutdated {
			downloadOverpassJSON(overpassNameURL, overpassNameResultFileName)
		}
		var overpassNameData OverpassResult
		err := readJSON(&overpassNameData, overpassNameResultFileName)
		if err != nil {
			log.Println(err)
		}
		if len(overpassNameData.Elements) > 0 && overpassNameData.Elements[0].Tags.Name != "" {
			analysis.AreaAsOsmObjectName = overpassNameData.Elements[0].Tags.Name
			analysis.AreaAsOsmObjectLatitude = overpassNameData.Elements[0].Center.Lat
			analysis.AreaAsOsmObjectLongitude = overpassNameData.Elements[0].Center.Lon
			if *d {
				log.Printf("found OSM object's name **%s**\n", analysis.AreaAsOsmObjectName)
			}
		}
	}
	if isAnalysisOutdated || isOverpassResultOutdated {
		timeForAnalyisStart := time.Now()
		c := http.Client{
			Timeout: timeOutAnalyzingOneURL * time.Second,
			CheckRedirect: func(req *http.Request, via []*http.Request) error {
				return http.ErrUseLastResponse
			},
		}
		analysis.Title = searchAreaID
		analysis.FaultyWebsites = []FaultyWebsite{}
		analysis.ChangesetComment = defaultChangesetComment
		analysis.ChangesetHashtags = url.QueryEscape(defaultChangesetHashtags)
		analysis.CustomStatusCodes = customStatusCodes
		analysis.TimeForOverpass = timeForOverpass
		analysis.MaxCharsForUrlName = maxCharsForUrlName
		analysis.IgnoreListOfURLs = ignoreList
		sumOfURLs := 0
		for i := 0; i < len(overpassData.Elements); i++ {
			analyzeOsmObject(&analysis, overpassData.Elements[i], &c, &sumOfURLs)
			if *dd && i%100 == 0 {
				log.Println("Analyzing object no.", i, "out of", len(overpassData.Elements), "total objects")
			}
		}
		analysis.TotalOsmObjectsAnalyzed = len(overpassData.Elements)
		analysis.TotalURLsAnalyzed = sumOfURLs
		analysis.AnalysisGenDate = time.Now()
		analysis.TimeForAnalysis = time.Since(timeForAnalyisStart)
		collectStatusCodeSums(&analysis)
		statsFileName := olvConfig.OutputPath + string(os.PathSeparator) + "statistics-" + searchAreaID + ".json"
		stats, err := generateStatsFromAnalysis(searchAreaID, &analysis, statsFileName)
		if err != nil {
			log.Println("Generating the statistics failed:", err)
		}
		chartJsData, err := generateChartJsStats(stats, statusColors)
		if err != nil {
			log.Println("Converting the statistics to chart.js format failed:", err)
		}
		analysis.ChartJsData = chartJsData
	} else {
		err := readJSON(&analysis, analysisFileName)
		if err != nil {
			log.Println(err)
		}
	}
	if *dd {
		log.Println("found", len(overpassData.Elements), "OSM objects with an URL, which have", analysis.TotalURLsAnalyzed, "URLs tagged")
	}
	if *d {
		log.Println("faulty URLs found by the analysis:", len(analysis.FaultyWebsites))
	}
	// search for POIs without any URL
	if *dd {
		log.Println("searching for POIs without URL now")
	}
	overpassPoiNoUrlsResultFileName := olvConfig.CachePath + string(os.PathSeparator) + defaultOverpassResultFileNamePrefix + "nourls-" + searchAreaID + defaultOverpassResultFileNamePostfix
	overpassPoiHasNoURL := defaultOverpassAreaPrefix + searchAreaID + defaultOverpassHasNoURLpostfixFirst + defaultOverpassFeaturesNoURL + defaultOverpassHasNoURLpostfixLast
	if *dd {
		log.Println("used overpass API call", overpassPoiHasNoURL)
	}
	var timeForOverpassNoURL time.Duration
	isOverpassResultNoURLsOutdated := isDataOutdated(overpassPoiNoUrlsResultFileName, defaultOverpassDataMaxAgeInHours)
	if isOverpassResultNoURLsOutdated {
		timeForOverpassNoURLStart := time.Now()
		downloadOverpassJSON(overpassPoiHasNoURL, overpassPoiNoUrlsResultFileName)
		timeForOverpassNoURL = time.Since(timeForOverpassNoURLStart)
	}
	// read overpass-api json result
	var overpassPoiNoUrlsData OverpassResult
	err = readJSON(&overpassPoiNoUrlsData, overpassPoiNoUrlsResultFileName)
	if err != nil {
		log.Fatalln(err)
	}
	// empty the POIsNoURL array to not create duplicates when reusing the previous analysis
	analysis.POIsNoURL = []POIwithoutURL{}
	for i := 0; i < len(overpassPoiNoUrlsData.Elements); i++ {
		osmObjectWithoutURL(&analysis, overpassPoiNoUrlsData.Elements[i])
	}
	if len(overpassPoiNoUrlsData.Elements) == 0 {
		log.Fatalln("Overpass returned a result with no OSM objects. Investigate or delete file", overpassPoiNoUrlsResultFileName)
	} else {
		if *d {
			log.Println("no. POIs without any URL:", len(overpassPoiNoUrlsData.Elements))
		}
	}
	analysis.ChangesetCommentAddURL = defaultChangesetCommentAddURL
	analysis.TimeForOverpassPOIsNoURL = timeForOverpassNoURL
	analysis.TotalPOIsMissingURL = len(overpassPoiNoUrlsData.Elements)

	analysis.ColorStatusCodes = statusColors
	analysis.GeneratingPerson = olvConfig.OsmUserName
	analysis.Version = version
	analysis.WebOutputSubdir = olvConfig.WebOutputSubdir
	analysis.PageGenDate = time.Now()
	err = writeNewJSON(analysis, analysisFileName)
	if err != nil {
		log.Println("could not write analysis result to disk:", err)
	}

	err = CopyFSDir(templateFiles, "tmpl/static", olvConfig.OutputPath)
	if err != nil {
		log.Println("error while copying contents from tmpl/static dir to", olvConfig.OutputPath, "dir. Error:", err)
	}
	writeTemplateToHTML(&analysis, templateNameFaultyWebsites, olvConfig.OutputPath)
	writeTemplateToHTML(&analysis, templateNameStatistics, olvConfig.OutputPath)
	writeTemplateToHTML(&analysis, templateNameMap, olvConfig.OutputPath)
	writeTemplateToHTML(&analysis, templateNameAbout, olvConfig.OutputPath)
	writeTemplateToHTML(&analysis, templateNamePOIsNoURL, olvConfig.OutputPath)
	createStatusIcons(statusColors, templateNameMarker, olvConfig.OutputPath)
	if *d {
		log.Printf("printElapsedTime: time elapsed %.2fs\n", time.Since(start).Seconds())
	}
}
