package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
)

const defaultSearchArea = 3601438081 // Altefähr
const defaultRelation = 0            // default is zero, so that we try to use the area ID then
const defaultConfigPath = "config.json"

var givenSearchAreaID = flag.Int64("area", defaultSearchArea, "Overpass API area ID as documented in https://wiki.openstreetmap.org/wiki/Overpass_API/Language_Guide#Area_clauses_(%22area_filters%22). Usually this is the relation ID plus 3600000000 or the way ID plus 2400000000, e.g. Munich relation ID is 62428 and thus the search area ID is 3600062428.\nIf no search area ID is given, the default ID 3601438081, representing Altefähr, is used.")
var givenRelationID = flag.Int64("rel", defaultRelation, "Relation ID of the OSM object. This parameter is preferred compared to -area, e.g. when both parameters are given.")
var givenConfig = flag.String("config", defaultConfigPath, "path to your config.json file")
var cleanIgnoreListFlag = flag.Bool("clean-ignore-list", false, "clean the ignore list only and exit")
var v = flag.Bool("v", false, "print the program's version")
var d = flag.Bool("d", false, "print debug output")
var dd = flag.Bool("dd", false, "like parameter -d, but print more debug output")
var ddd = flag.Bool("ddd", false, "like parameter -dd, but print even more debug output")

var searchAreaID string

func handleFlags() {
	flag.Parse()
	// version first, because it directly exits here
	if *v {
		fmt.Printf("version %v\n", version)
		os.Exit(0)
	}

	if !*cleanIgnoreListFlag {
		if *givenRelationID > 0 {
			i := *givenRelationID + 3600000000
			searchAreaID = strconv.FormatInt(i, 10)
			if *dd {
				log.Println("relation ID converted to a searchAreaID:", searchAreaID)
			}
		} else if *givenSearchAreaID > 0 {
			i := *givenSearchAreaID
			searchAreaID = strconv.FormatInt(i, 10)
			if *dd {
				log.Println("searchAreaID", searchAreaID)
			}
		} else {
			log.Fatalln("given ID is less or equal than 0 and thus not allowed")
			os.Exit(1)
		}
	}

	if *ddd {
		*dd = true
		*d = true
		log.Println("Debug Level ddd is active")
	} else if *dd {
		*d = true
		log.Println("Debug Level dd is active")
	} else if *d {
		log.Println("Debug Level d is active")
	}

}
