package main

import (
	"embed"
	"log"
	"net/url"
	"os"
	"strconv"
	"strings"
	"text/template"
	"time"
)

const templateNameFaultyWebsites = "table"
const templateNameStatistics = "chart"
const templateNameMap = "map"
const templateNameAbout = "about"
const templateNameMarker = "marker"
const templateNamePOIsNoURL = "pois-no-url"
const tmplDirectory = "tmpl"
const tmplSuffix = ".go.tmpl"

//go:embed all:tmpl
var templateFiles embed.FS

func writeTemplateToHTML(analysis *Analysis, templateName string, outputDir string) {
	if _, err := os.Stat(outputDir); os.IsNotExist(err) {
		os.Mkdir(outputDir, os.ModePerm)
	}
	f, err := os.Create(outputDir + string(os.PathSeparator) + templateName + "-" + searchAreaID + ".html")
	if err != nil {
		log.Println("writeTemplateToHTML", err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Println("writeTemplateToHTML", err)
		}
	}()

	htmlSource, err := template.New(templateName+tmplSuffix).Funcs(template.FuncMap{
		"getFirstCharOfString": func(input string) string {
			if len(input) > 0 {
				return input[:1]
			}
			return input
		},
		"escapeForJS": func(input string) string {
			noBacksticks := strings.ReplaceAll(input, "`", "&#96;")
			noBackslashes := strings.ReplaceAll(noBacksticks, "\\", "\\\\")
			noNewlines := strings.ReplaceAll(noBackslashes, "\n", "")
			noApostrophs := strings.ReplaceAll(noNewlines, "'", "\\'")
			return strings.ReplaceAll(noApostrophs, "\"", "\\\"")
		},
		"formatTime": func(t time.Time) string {
			return t.Format("2006-01-02 15:04:05 -07:00")
		},
		"removeColon": func(input string) string {
			return strings.ReplaceAll(input, ":", "")
		},
		"urlEncode": func(input string) string {
			return url.QueryEscape(input)
		},
		"arr": func(els ...any) []any {
			return els
		},
	}).ParseFS(templateFiles, tmplDirectory+"/*"+tmplSuffix)
	if err != nil {
		log.Println("writeTemplateToHTML", err)
	}
	if htmlSource == nil {
		log.Println("writeTemplateToHTML: template parsing failed")
	}
	analysis.TemplateName = templateName
	htmlSource.Execute(f, analysis)
}

func createStatusIcons(colorPalette map[int]string, templateName string, outputDir string) {
	markerIconsDir := outputDir + string(os.PathSeparator) + cssDir + string(os.PathSeparator) + imagesDir
	if _, err := os.Stat(markerIconsDir); os.IsNotExist(err) {
		os.Mkdir(markerIconsDir, os.ModePerm)
	}
	for statusCode, color := range colorPalette {
		f, err := os.Create(markerIconsDir + string(os.PathSeparator) + templateName + "-" + strconv.Itoa(statusCode) + ".svg")
		if err != nil {
			log.Println("createStatusIcons", err)
		}
		defer func() {
			if err := f.Close(); err != nil {
				log.Println("createStatusIcons", err)
			}
		}()
		svgSource, err := template.New(templateName+tmplSuffix).ParseFS(templateFiles, tmplDirectory+string(os.PathSeparator)+templateName+tmplSuffix)
		if err != nil {
			log.Println("createStatusIcons", err)
		}
		if svgSource == nil {
			log.Println("createStatusIcons: template parsing failed")
		}
		svgSource.Execute(f, color)
	}
}
