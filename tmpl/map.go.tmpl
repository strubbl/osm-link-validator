{{template "header.go.tmpl" .}}
<title>Map about faulty URLs in area {{ if ne .AreaAsOsmObjectName "" }}{{ .AreaAsOsmObjectName }} ({{ end }}{{ if ne .AreaAsOsmObjectID 0 }}{{ .AreaAsOsmObjectID }}{{ else }}{{ .Title }}{{ end }}{{ if ne .AreaAsOsmObjectName "" }}){{ end }} · OpenStreetMap Link Validator</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="stylesheet" href="css/leaflet.css" />
<link rel="stylesheet" href="css/MarkerCluster.css" />
<link rel="stylesheet" href="css/MarkerCluster.FaultyURLs.css" />
<link rel="stylesheet" href="css/L.Control.Locate.min.css" />
<link rel="stylesheet" href="css/Infobox.css" />
<link rel="stylesheet" href="css/Control.Geocoder.css" />
<script src="js/leaflet.js"></script>
<script src="js/leaflet.markercluster.js"></script>
<script src="js/leaflet.markercluster.layersupport.js"></script>
<script src="js/leaflet-hash.js"></script>
<script src="js/L.Control.Locate.min.js"></script>
<script src="js/leaflet.infobox.js"></script>
<script src="js/Control.Geocoder.js"></script>
<style type="text/css">
  #map {
    position: absolute;
    height: 85%;
    width: 99%;
  }
</style>
<script type="text/javascript">
  const isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;
  const allStatusCodes = new Map([
    [301, "Moved Permanently"],
    [302, "Found"],
    [303, "See Other"],
    [304, "Not Modified"],
    [305, "Use Proxy"],
    [306, "Switch Proxy"],
    [307, "Temporary Redirect"],
    [308, "Permanent Redirect"],
    [400, "Bad Request"],
    [401, "Unauthorized"],
    [402, "Payment Required"],
    [403, "Forbidden"],
    [404, "Not Found"],
    [405, "Method Not Allowed"],
    [406, "Not Acceptable"],
    [407, "Proxy Authentication Required"],
    [408, "Request Timeout"],
    [409, "Conflict"],
    [410, "Gone"],
    [411, "Length Required"],
    [412, "Precondition Failed"],
    [413, "Payload Too Large"],
    [414, "URI Too Long"],
    [415, "Unsupported Media Type"],
    [416, "Range Not Satisfiable"],
    [417, "Expectation Failed"],
    [418, "I'm a teapot"],
    [421, "Misdirected Request"],
    [429, "Too Many Requests"],
    [500, "Internal Server Error"],
    [501, "Not Implemented"],
    [502, "Bad Gateway"],
    [503, "Service Unavailable"],
    [504, "Gateway Timeout"],
    [505, "HTTP Version Not Supported"],
    [506, "Variant Also Negotiates"],
    [507, "Insufficient Storage"],
    [508, "Loop Detected"],
    [510, "Not Extended"],
    [511, "Network Authentication Required"],
    [530, "Site is frozen"],
    {{ range $key, $value := $.CustomStatusCodes }}[{{ $key }}, "{{ $value }}"],
    {{ end }}
    []
  ])
  var icon_folder = 'css/images'
  var StatusIcon = L.Icon.extend({
    options: {
      iconSize: [25, 41],
      iconAnchor: [12, 40],
      popupAnchor: [0, -42]
    }
  });
  var icon_default = new StatusIcon({iconUrl: icon_folder + '/marker.svg'})
  {{ range $key, $value := $.ColorStatusCodes }}
    var icon_{{ $key }} = new StatusIcon({iconUrl: icon_folder + '/marker-{{ $key }}.svg'}) // color={{ $value }}
  {{ end }}

  function faultyURLsMap() {
    // add an OpenStreetMap tile layer
    var osm = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors, OSM-Link-Validator <a href="https://codeberg.org/strubbl/osm-link-validator/releases">v{{ .Version }}</a>, page generated at {{ formatTime .PageGenDate }}',
        maxZoom: 19
    })
    // create a map in the "map" div, set the view to a given place and zoom
    var map = L.map('map', {
      center: [{{ .AreaAsOsmObjectLatitude }}, {{ .AreaAsOsmObjectLongitude }}],
      zoom: 12,
      layers: osm
    })

    var isLegendCollapsed = false
    if (isMobile) {
      isLegendCollapsed = true
    }
    var layerControl = L.control.layers({"OpenStreetMap": osm}, null, {collapsed: isLegendCollapsed, sortLayers: true}).addTo(map);
    var mcgLayerSupportGroup = L.markerClusterGroup.layerSupport({showCoverageOnHover: false, maxClusterRadius: 40})
    mcgLayerSupportGroup.addTo(map);

    var mapStatusToMarkers = new Map();
    // list all POIs with faulty URL
    var headline = ""
    var title = ""
    var editlinkID = ""
    var editlinkJOSM = ""
    var analysistext = ""
    var markerIcon = ""
    {{range $f := .FaultyWebsites}}
      var scMarkers = mapStatusToMarkers.get('{{ .StatusCode }}')
      if (scMarkers === undefined) {
        scMarkers = []
      }
      headline = `{{template "link-osm-object.go.tmpl" .}} <b>{{ if and (eq .Name "") (ne .Operator "") }}Operator: {{ escapeForJS .Operator }}{{ else }}{{ escapeForJS .Name }}{{ end }}</b>`
      title = "{{ if and (eq .Name "") (ne .Operator "") }}Operator: {{ escapeForJS .Operator }}{{ else }}{{ escapeForJS .Name }}{{ end }}"
      editlinkID = `{{ template "link-editor-iD.go.tmpl" (arr .Type .ID .Lat .Lon $.ChangesetComment $.ChangesetHashtags) }}`
      editlinkL0 = `{{ template "link-editor-level0.go.tmpl" (arr .Type .ID $.ChangesetComment $.ChangesetHashtags) }}`
      editlinkJOSM = `{{ template "link-editor-josm.go.tmpl" (arr $.ChangesetComment $.ChangesetHashtags .Type .ID) }}`
      analysistext = '{{ escapeForJS .TagType }} has the URL<br /><a href="{{ escapeForJS .URL }}" target="_blank">{{ escapeForJS .URL }}</a><br />reporting the status<br />{{ if eq .Status "" }}{{ .StatusCode }}{{ else }}{{ escapeForJS .Status }}{{ end }}<br />{{ if ne .FinalURL "" }}Redirect URL: <a href="{{ escapeForJS .FinalURL }}" target="_blank">{{ escapeForJS .FinalURL }}</a>{{ end }}<br />{{ escapeForJS .Comment }}'
      markerIcon = icon_default
      {{ range $key, $value := $.ColorStatusCodes }} {{ if eq $f.StatusCode $key }} markerIcon = icon_{{ $f.StatusCode }} {{ end }} {{ end }}
      curMarker = L.marker(
          [{{ .Lat }}, {{ .Lon }}],
          {"title": title,
          "icon": markerIcon
          })
        .bindPopup(headline + "<hr/>"+ editlinkID + " " + editlinkJOSM + " " + editlinkL0 + "<hr/>" + analysistext)

      scMarkers.push(curMarker)
      mapStatusToMarkers.set('{{ .StatusCode }}', scMarkers)
    {{ end }}

    const legendLabels = new Map()
    {{ range $key, $value := $.ColorStatusCodes }}
    var statusText = allStatusCodes.get({{ $key }})
    if (statusText === undefined) {
      statusText = ""
    }
    legendLabels.set('{{ $key }}', '<!-- {{ $key }} --><img src="' + icon_folder + '/marker-{{ $key }}.svg" height="22" /> {{ $key }} ' + statusText)
    {{ end }}

    mapStatusToMarkers.forEach((value, key) => {
      var codeMarkers = L.layerGroup(value)
      var label = key
      tmpLabel = legendLabels.get(key)
      if (tmpLabel === undefined) {
        label = '<!-- ' + key + ' --><img src="' + icon_folder + '/marker.svg" height="22" /> ' + key
      }
      else {
        label = tmpLabel
      }
      layerControl.addOverlay(codeMarkers, label)
      // activate the overlay
      mcgLayerSupportGroup.checkIn(codeMarkers)
      codeMarkers.addTo(map)
    })

    // Locate Button
    L.control.locate({icon: "fa-solid fa-location-dot", iconLoading: "fa-solid fa-spinner"}).addTo(map)

//    // Infobox
//    L.control.infobox().addTo(map)
//    var closebar = document.getElementById("closebar")
//    closebar.onclick = function() {
//      document.getElementById("infobox").style.display = "none"
//    }

    // Search
    L.Control.geocoder({position: 'topleft', placeholder: 'Search…', errorMessage: 'No results found.'}).addTo(map)

    // Scale
    scale = L.control.scale()
    scale.setPosition('bottomright')
    scale.addTo(map)

    // Hash URL
    var hash = new L.Hash(map)
  }

  document.addEventListener('DOMContentLoaded', faultyURLsMap, false)
</script>
</head>
<body>
  {{template "navigation.go.tmpl" .}}
  <h1><i18n vanilla-i18n="map.headline">Map about faulty URLs in area</i18n>
    {{ if ne .AreaAsOsmObjectName "" }}
      {{ .AreaAsOsmObjectName }} (
    {{ end }}
    {{ if ne .AreaAsOsmObjectID 0 }}
      <a href="https://osm.org/{{ .AreaAsOsmObjectType }}/{{ .AreaAsOsmObjectID }}">{{ .AreaAsOsmObjectID }}</a>
    {{ else }}
      {{ .Title }}
    {{ end }}
    {{ if ne .AreaAsOsmObjectName "" }}){{ end }}
  </h1>

  <div id="map"></div>

  <!--div id="infobox">
    <h1>OSM-Link-Validator</h1>
    <h2><i18n vanilla-i18n="map.legend.headline">Legend</i18n></h2>
    <p>
      <i18n vanilla-i18n="map.legend.description">The symbols in the map have the following meaning</i18n>: <br />
      <img src="icons/marker-icon.png" alt="<i18n vanilla-i18n="map.legend.marker1.description">Marker</i18n>" /> <i18n vanilla-i18n="map.legend.marker1.description">This POI has a problem with one of its tagged URLs.</i18n></br >
    </p>
    <div id="closebar"><i18n vanilla-i18n="map.close">Close</i18n></div>
  </div-->

</body>
</html>
