package main

import (
	"encoding/json"
	"strconv"
	"testing"
)

func TestCheckForCommonRedirect(t *testing.T) {
	t.Parallel()
	url1 := "http://strubbl.de"
	url2 := "http://strubbl.de/"
	url3 := "http://strubbl.de/de/"
	url4 := "http://strubbl.de/de"
	url5 := "http://strubbl.de/en/"
	url6 := "http://strubbl.de/en"
	url7 := "http://strubbl.de/index.php/de/"
	url8 := "http://strubbl.de/DE"
	url9 := "http://www.strubbl.de"
	url10 := "https://strubbl.de"
	url11 := "https://www.strubbl.de"
	const commonRedirectErrMsgPrefix = "common redirect got not detected: "

	if checkForCommonRedirect(url1, url1) != false {
		t.Errorf(commonRedirectErrMsgPrefix + "original URL equals final URL")
	}

	if checkForCommonRedirect(url1, url2) != true {
		t.Errorf(commonRedirectErrMsgPrefix + "ending slash in final URL")
	}
	if checkForCommonRedirect(url2, url1) != true {
		t.Errorf(commonRedirectErrMsgPrefix + "ending slash in original URL")
	}

	if checkForCommonRedirect(url1, url3) != true {
		t.Errorf(commonRedirectErrMsgPrefix + "language suffix incl. slash")
	}
	if checkForCommonRedirect(url2, url3) != true {
		t.Errorf(commonRedirectErrMsgPrefix + "language suffix incl. slash")
	}

	if checkForCommonRedirect(url1, url4) != true {
		t.Errorf(commonRedirectErrMsgPrefix + "language suffix without slash")
	}
	if checkForCommonRedirect(url2, url4) != true {
		t.Errorf(commonRedirectErrMsgPrefix + "language suffix without slash")
	}

	if checkForCommonRedirect(url1, url5) != true {
		t.Errorf(commonRedirectErrMsgPrefix + "language suffix incl. slash")
	}
	if checkForCommonRedirect(url2, url6) != true {
		t.Errorf(commonRedirectErrMsgPrefix + "language suffix incl. slash")
	}

	if checkForCommonRedirect(url1, url5) != true {
		t.Errorf(commonRedirectErrMsgPrefix + "language suffix without slash")
	}
	if checkForCommonRedirect(url2, url6) != true {
		t.Errorf(commonRedirectErrMsgPrefix + "language suffix without slash")
	}

	if checkForCommonRedirect(url1, url7) != true {
		t.Errorf(commonRedirectErrMsgPrefix + "language suffix index.php without slash")
	}
	if checkForCommonRedirect(url2, url7) != true {
		t.Errorf(commonRedirectErrMsgPrefix + "language suffix index.php with slash")
	}
	if checkForCommonRedirect(url2, url8) != true {
		t.Errorf(commonRedirectErrMsgPrefix + "language suffix in capital letters")
	}
	if checkForCommonRedirect(url1, url9) != true {
		t.Errorf(commonRedirectErrMsgPrefix + "to http://www.")
	}
	if checkForCommonRedirect(url9, url1) != true {
		t.Errorf(commonRedirectErrMsgPrefix + "from http://www.")
	}
	if checkForCommonRedirect(url10, url11) != true {
		t.Errorf(commonRedirectErrMsgPrefix + "to https://www.")
	}
	if checkForCommonRedirect(url11, url10) != true {
		t.Errorf(commonRedirectErrMsgPrefix + "from https://www.")
	}
	if checkForCommonRedirect(url2, url9) != true {
		t.Errorf(commonRedirectErrMsgPrefix + "from http://www. with slash to no slash")
	}
	if checkForCommonRedirect(url9, url2) != true {
		t.Errorf(commonRedirectErrMsgPrefix + "to http://www. with slash from no slash")
	}
}

func TestCheckForUrlShorteners(t *testing.T) {
	t.Parallel()
	testURLs := [...]string{"http://bit.ly/asdf", "https://bit.ly/asdf", "http://goo.gl/asdf", "http://g.co/asdf", "http://ow.ly/asdf", "http://t.co/asdf", "http://tinyurl.com/asdf", "http://w.wiki/asdf", "http://youtu.be/asdf", "https://photos.app.goo.gl/drnfiounjwerufnuoi3p4rn3o"}
	for i := 0; i < len(testURLs); i++ {
		isUrlShort, _ := checkForUrlShorteners(testURLs[i])
		if isUrlShort != true {
			t.Errorf("URL shortener not detected: %v", testURLs[i])
		}
	}
}

func TestCheckForTrackingParameter(t *testing.T) {
	t.Parallel()
	testURLs := [...]string{"http://bit.ly/?mc_id=asdf", "https://bit.ly/?asdf=ghjk&mc_id=asdf"}
	for i := 0; i < len(testURLs); i++ {
		isTracking, _ := checkForTrackingParameter(testURLs[i])
		if isTracking != true {
			t.Errorf("URL shortener not detected: %v", testURLs[i])
		}
	}
}

func TestCheckForSocialMedia(t *testing.T) {
	t.Parallel()
	testUrlsTrue := [...]string{"http://facebook.com/asdf", "https://www.youtube.com/?asdf=ghjk&mc_id=asdf"}
	for i := 0; i < len(testUrlsTrue); i++ {
		isTracking, _ := checkForSocialMedia(testUrlsTrue[i])
		if isTracking != true {
			t.Errorf("URL shortener not detected: %v", testUrlsTrue[i])
		}
	}
	testUrlsFalse := [...]string{"http://google.de/asdf", "https://strubbl.de/?asdf=ghjk&mc_id=asdf"}
	for i := 0; i < len(testUrlsFalse); i++ {
		isTracking, _ := checkForSocialMedia(testUrlsFalse[i])
		if isTracking != false {
			t.Errorf("URL shortener not detected: %v", testUrlsFalse[i])
		}
	}
}

func TestCheckForIgnoreList(t *testing.T) {
	t.Parallel()
	var ignoreList IgnoreList
	startURL1 := "http://strubbl.de/"
	finalURL1 := "https://strubbl.de/"
	finalURL2 := "https://strubbl.de"
	status1 := 301
	iLJson1 := "{\"Entries\": [{\"Status\": " + strconv.Itoa(status1) + ",\"StartURL\": \"" + startURL1 + "\",\"FinalURL\": \"\"}]}"
	err := json.Unmarshal([]byte(iLJson1), &ignoreList)
	if err != nil {
		t.Errorf("cannot unmarshall json: %v", err)
	}
	if true != checkForIgnoreList(status1, startURL1, "", &ignoreList) {
		t.Errorf("Ignore list entry with empty FinalURL not detected: %d %v %v", status1, startURL1, finalURL1)
	}
	if true != checkForIgnoreList(status1, startURL1, finalURL1, &ignoreList) {
		t.Errorf("Ignore list entry with empty FinalURL not detected: %d %v %v", status1, startURL1, finalURL1)
	}
	if true == checkForIgnoreList(status1, finalURL1, startURL1, &ignoreList) {
		t.Errorf("Ignore list entry with empty FinalURL detected although it is not in the list: %d %v %v", status1, finalURL1, startURL1)
	}

	iLJson2 := "{\"Entries\": [{\"Status\": " + strconv.Itoa(status1) + ",\"StartURL\": \"" + startURL1 + "\",\"FinalURL\": \"" + finalURL1 + "\"}]}"
	err = json.Unmarshal([]byte(iLJson2), &ignoreList)
	if err != nil {
		t.Errorf("cannot unmarshall json: %v", err)
	}
	if true != checkForIgnoreList(status1, startURL1, finalURL1, &ignoreList) {
		t.Errorf("Ignore list entry not detected: %d %v %v", status1, startURL1, finalURL1)
	}
	if true == checkForIgnoreList(status1, startURL1, "", &ignoreList) {
		t.Errorf("Ignore list entry with given FinalURL detected although the finalURL to check is empty: %d %v %v", status1, startURL1, "")
	}
	// check for trailing slash being ignored
	if true != checkForIgnoreList(status1, startURL1, finalURL2, &ignoreList) {
		t.Errorf("Ignore list entry not detected due to a slash: %d %v %v", status1, startURL1, finalURL2)
	}
	// with the URL without slash being in the ignore list
	iLJson3 := "{\"Entries\": [{\"Status\": " + strconv.Itoa(status1) + ",\"StartURL\": \"" + startURL1 + "\",\"FinalURL\": \"" + finalURL2 + "\"}]}"
	err = json.Unmarshal([]byte(iLJson3), &ignoreList)
	if err != nil {
		t.Errorf("cannot unmarshall json: %v", err)
	}
	if true != checkForIgnoreList(status1, startURL1, finalURL1, &ignoreList) {
		t.Errorf("Ignore list entry not detected due to a slash: %d %v %v", status1, startURL1, finalURL1)
	}
}
