package main

// type definitions being used in an Overpass-API JSON result
type OverpassResult struct {
	Version   float64   `json:"version,omitempty"`
	Generator string    `json:"generator"`
	Elements  []Element `json:"elements"`
}

type Element struct {
	Type   string  `json:"type"` // only node, way, relation
	ID     int64   `json:"id"`
	Lat    float64 `json:"lat"`
	Lon    float64 `json:"lon"`
	Center Center  `json:"center"`
	Tags   Tags    `json:"tags"`
}

type Center struct {
	Lat float64 `json:"lat"`
	Lon float64 `json:"lon"`
}

type Tags struct {
	AddrCity        string `json:"addr:city"`
	AddrCountry     string `json:"addr:country"`
	AddrHousenumber string `json:"addr:housenumber"`
	AddrPostcode    string `json:"addr:postcode"`
	AddrStreet      string `json:"addr:street"`
	AddrSuburb      string `json:"addr:suburb"`
	Amenity         string `json:"amenity"`
	BrandWebsite    string `json:"brand:website"`
	ContactEmail    string `json:"contact:email"`
	ContactWebsite  string `json:"contact:website"`
	DisusedAmenity  string `json:"disused:amenity"`
	DisusedShop     string `json:"disused:shop"`
	DisusedTourism  string `json:"disused:tourism"`
	Email           string `json:"email"`
	HeritageWebsite string `json:"heritage:website"`
	Image           string `json:"image"`
	Leisure         string `json:"leisure"`
	MemorialWebsite string `json:"memorial:website"`
	Name            string `json:"name"`
	NetworkWebsite  string `json:"network:website"`
	Operator        string `json:"operator"`
	OperatorWebsite string `json:"operator:website"`
	PaymentWebsite  string `json:"payment:website"`
	Shop            string `json:"shop"`
	SourceWebsite   string `json:"source:website"`
	Tourism         string `json:"tourism"`
	URL             string `json:"url"`
	Website         string `json:"website"`
	WebsiteBooking  string `json:"website:booking"`
	WebsiteMap      string `json:"website:map"`
	WebsiteMenu     string `json:"website:menu"`
	WebsiteSource   string `json:"website:source"`
	WebsiteStock    string `json:"website:stock"`
	XmasURL         string `json:"xmas:url"`
}
