package main

import (
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

// HST 3600026225
// M 3600062428
// Altefähr 3601438081
const defaultOverpassAreaPrefix = "https://overpass-api.de/api/interpreter?data=[out:json][timeout:300];area(id:"
const defaultOverpassURLpostfix = ")->.searchArea;(nwr[\"contact:website\"](area.searchArea);nwr[\"website\"](area.searchArea);nwr[\"url\"](area.searchArea);nwr[\"image\"](area.searchArea);nwr[\"brand:website\"](area.searchArea);nwr[\"heritage:website\"](area.searchArea);nwr[\"memorial:website\"](area.searchArea);nwr[\"network:website\"](area.searchArea);nwr[\"operator:website\"](area.searchArea);nwr[\"payment:website\"](area.searchArea);nwr[\"website:booking\"](area.searchArea);nwr[\"website:map\"](area.searchArea);nwr[\"website:menu\"](area.searchArea);nwr[\"website:stock\"](area.searchArea);nwr[\"source:website\"](area.searchArea);nwr[\"xmas:url\"](area.searchArea););out%20center;"
const defaultOverpassHasNoURLpostfixFirst = ")->.searchArea;("
const defaultOverpassHasNoURLpostfixLast = ");out%20center;"
const defaultOverpassIncludeNameExcludeURLs = "[\"name\"][\"website\"!~\".*\"][\"contact:website\"!~\".*\"][\"url\"!~\".*\"][\"contact:facebook\"!~\".*\"][\"contact:instagram\"!~\".*\"][\"contact:youtube\"!~\".*\"][\"contact:tiktok\"!~\".*\"](area.searchArea);"
const defaultOverpassHasNoURLCafe = "nwr[\"amenity\"=\"cafe\"]" + defaultOverpassIncludeNameExcludeURLs
const defaultOverpassHasNoURLHotel = "nwr[\"tourism\"=\"hotel\"]" + defaultOverpassIncludeNameExcludeURLs
const defaultOverpassHasNoURLMarina = "nwr[\"leisure\"=\"marina\"]" + defaultOverpassIncludeNameExcludeURLs
const defaultOverpassHasNoURLSchool = "nwr[\"amenity\"=\"school\"]" + defaultOverpassIncludeNameExcludeURLs
const defaultOverpassHasNoURLKiga = "nwr[\"amenity\"=\"kindergarten\"]" + defaultOverpassIncludeNameExcludeURLs
const defaultOverpassHasNoURLRestaurant = "nwr[\"amenity\"=\"restaurant\"]" + defaultOverpassIncludeNameExcludeURLs
const defaultOverpassFeaturesNoURL = defaultOverpassHasNoURLCafe + defaultOverpassHasNoURLHotel + defaultOverpassHasNoURLMarina + defaultOverpassHasNoURLSchool + defaultOverpassHasNoURLKiga + defaultOverpassHasNoURLRestaurant
const defaultOverpassResultFileNamePrefix = "overpass-"
const defaultOverpassResultFileNamePostfix = ".json"
const defaultUserAgentForOverpass = "codeberg.org/strubbl/osm-link-validator"
const overpassRelationOffset = 3600000000
const overpassWayOffset = 2400000000

func downloadOverpassJSON(overpassURL string, fileName string) {
	downloadClient := http.Client{
		Timeout: time.Second * 360,
	}
	r, err := http.NewRequest("GET", overpassURL, nil)
	if err != nil {
		log.Println(err)
	}
	r.Header.Set("User-Agent", defaultUserAgentForOverpass)
	resp, err := downloadClient.Do(r)
	if err != nil {
		log.Println(err)
	}
	defer resp.Body.Close()
	file, err := os.Create(fileName)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	size, err := io.Copy(file, resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	if size == 0 {
		log.Fatalln("Overpass result file has the size of 0")
	}
	if *dd {
		log.Printf("Downloaded an overpass file %s with size %d\n", fileName, size)
	}
}

func getOsmObjectIdFromAreaId(areaID int64) (objectID int64, objectType string) {
	if *dd {
		log.Println("getOsmObjectIdFromAreaId: trying to resolve OSM object ID from the search area ID", areaID)
	}
	returnAreaID := areaID
	returnAreaIdObjectType := ""
	if areaID-overpassRelationOffset > 0 {
		returnAreaID = areaID - overpassRelationOffset
		returnAreaIdObjectType = "relation"
		return returnAreaID, returnAreaIdObjectType
	} else if areaID-overpassWayOffset > 0 {
		returnAreaID = areaID - overpassWayOffset
		returnAreaIdObjectType = "way"
	} else {
		if *d {
			log.Println("could not resolve OSM object ID from the search area ID", areaID)
		}
	}
	return returnAreaID, returnAreaIdObjectType
}
