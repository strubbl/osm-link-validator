package main

import (
	"embed"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
)

func CopyFSDir(f embed.FS, src string, dst string) error {
	if _, err := os.Stat(dst); os.IsNotExist(err) {
		if err := os.MkdirAll(dst, os.ModePerm); err != nil {
			return fmt.Errorf("CopyFSDir: error creating directory %s: %v", dst, err)
		}
	}
	fileList, err := f.ReadDir(src)
	if err != nil {
		return fmt.Errorf("CopyFSDir: error reading directory %s: %v", src, err)
	}
	for _, file := range fileList {
		srcFileName := filepath.Join(src, file.Name())
		dstFileName := filepath.Join(dst, file.Name())
		if file.IsDir() {
			if err := CopyFSDir(f, srcFileName, dstFileName); err != nil {
				return fmt.Errorf("CopyFSDir: error copying subdirectory %s: %v", file.Name(), err)
			}
		} else {
			err = CopyFSFile(f, srcFileName, dstFileName)
			if err != nil {
				return fmt.Errorf("CopyFSDir: error while copying file %s from src to dst: %v", file.Name(), err)
			}
		}
	}
	return nil
}

func CopyFSFile(f embed.FS, src, dst string) (err error) {
	srcFile, err := f.Open(src)
	if err != nil {
		log.Println("CopyFSFile: src", src, "dst", dst, "error during open:", err)
		return nil
	}
	defer srcFile.Close()
	dstFile, err := os.Create(dst)
	if err != nil {
		if *ddd {
			// file might already exist
			log.Println("CopyFSFile: src", src, "dst", dst, "error during create:", err)
		}
		return nil
	}
	defer func() {
		if e := dstFile.Close(); e != nil {
			err = e
		}
	}()
	_, err = io.Copy(dstFile, srcFile)
	if err != nil {
		log.Println("CopyFSFile: src", src, "dst", dst, "error during copy:", err)
		return nil
	}
	err = dstFile.Sync()
	if err != nil {
		log.Println("CopyFSFile: src", src, "dst", dst, "error during sync:", err)
		return nil
	}
	err = os.Chmod(dst, 0644)
	if err != nil {
		log.Println("CopyFSFile: src", src, "dst", dst, "error during chmod:", err)
		return nil
	}
	return nil
}
