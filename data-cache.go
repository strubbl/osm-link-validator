package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"log"
	"os"
	"time"
)

const jsoIndentPrefix = ""
const jsoIndentString = "  "

func initCacheAndOutputDir(cacheDir string, outputDir string) {
	if _, err := os.Stat(cacheDir); os.IsNotExist(err) {
		os.Mkdir(cacheDir, os.ModePerm)
	}
	if _, err := os.Stat(outputDir); os.IsNotExist(err) {
		os.Mkdir(outputDir, os.ModePerm)
	}
}

func isDataOutdated(fileName string, maxAgeInHours int) (isOutdated bool) {
	testFile, err := os.Stat(fileName)
	if err != nil {
		if *dd {
			log.Println(err)
		}
		return true
	}
	notOlderThan := time.Now().Add(time.Hour * time.Duration(-1*maxAgeInHours))
	if *dd {
		log.Println("File", fileName, "modified time:", testFile.ModTime())
	}
	if testFile.ModTime().Before(notOlderThan) {
		if *dd {
			log.Println("File", fileName, "is too old and thus outdated (treshold time is", notOlderThan, ")")
		}
		return true
	}
	if *dd {
		log.Println("File", fileName, "is still fresh because it is newer than", notOlderThan)
	}
	return false
}

func readJSON(i interface{}, jsonFilePath string) error {
	if *ddd {
		log.Printf("readJSON: given type: %T\n", i)
	}
	switch i.(type) {
	case *OverpassResult:
		if *ddd {
			log.Println("readJSON: found *OverpassResult type")
		}
	case *Analysis:
		if *ddd {
			log.Println("readJSON: found *Analysis type")
		}
	case *Statistics:
		if *ddd {
			log.Println("readJSON: found *Statistics type")
		}
	case *IgnoreList:
		if *ddd {
			log.Println("readJSON: found *IgnoreList type")
		}
	case *OlvConfiguration:
		if *ddd {
			log.Println("readJSON: found *OlvConfiguration type")
		}
	default:
		log.Fatalln("readJSON: unkown type for reading json")
		return nil
	}

	if *dd {
		log.Printf("readJSON: jsonFilePath for type %T is %s\n", i, jsonFilePath)
	}
	if _, err := os.Stat(jsonFilePath); os.IsNotExist(err) {
		if *d {
			// in case file does not exist, we cannot prefill the data from json
			log.Printf("readJSON: file does not exist %s\n", jsonFilePath)
		}
		return nil
	}
	b, err := os.ReadFile(jsonFilePath)
	if err != nil {
		if *d {
			log.Println("readJSON: error while os.ReadFile of", jsonFilePath, "was", err)
		}
		return err
	}
	err = json.Unmarshal(b, i)
	if err != nil {
		if *d {
			log.Println("readJSON: error while json.Unmarshal of", jsonFilePath, "was", err)
		}
		return err
	}
	return nil
}

func writeNewJSON(i interface{}, jsonFilePath string) error {
	if *dd {
		log.Printf("writeNewJSON: given type: %T\n", i)
	}
	switch i.(type) {
	case Analysis:
		if *ddd {
			log.Println("writeNewJSON: found Analysis type")
		}
	case Statistics:
		if *ddd {
			log.Println("writeNewJSON: found Statistics type")
		}
	case IgnoreList:
		if *ddd {
			log.Println("writeNewJSON: found IgnoreList type")
		}
	case OlvConfiguration:
		if *ddd {
			log.Println("writeNewJSON: found OlvConfiguration type")
		}
	default:
		return errors.New("unkown data type for writing json")
	}

	b, err := json.MarshalIndent(i, jsoIndentPrefix, jsoIndentString)
	if err != nil {
		log.Println("writeNewJSON: error while marshalling data json", err)
		return err
	}
	if len(b) <= 0 {
		return errors.New("result of MarshalIndent is zero bytes, thus not writing the file " + jsonFilePath)
	}
	err = os.WriteFile(jsonFilePath, b, 0644)
	if err != nil {
		log.Println("writeNewJSON: error while writing data json", err)
		return err
	}
	return nil
}

func WriteNonEscapedJSON(t interface{}, jsonFilePath string) error {
	b := &bytes.Buffer{}
	encoder := json.NewEncoder(b)
	encoder.SetEscapeHTML(false)
	encoder.SetIndent(jsoIndentPrefix, jsoIndentString)
	err := encoder.Encode(t)
	if err != nil {
		return err
	}
	bt := bytes.TrimRight(b.Bytes(), "\n")
	err = os.WriteFile(jsonFilePath, bt, 0644)
	if err != nil {
		log.Println("WriteNonEscapedJSON: error while writing data json", err)
	}
	return err
}
