# OSM-Link-Validator

[![Übersetzungsstatus](https://translate.codeberg.org/widget/osm-link-validator/js-vanilla-i18n/287x66-grey.png)](https://translate.codeberg.org/engage/osm-link-validator/)

## Description

This software downloads OSM objects of an area via Overpass API and validates their links. Several HTML pages are generated, e.g. one with a table showing all the links, which have had a problem. A problem with a link is detected in case it does not respond with HTTP status 200, contains a tracking parameter or should use a different, more specialized OSM key. A map view of that analysis is also available.


## Example

An example of the result of this program for Munich can be found at https://osm.strubbl.de/olv/minga.html. The duration for creating this website with OSM-Link-Validator is about 4 hours.


## Dependencies

The software dependends on the [Overpass API](https://overpass-api.de/) and an online connection of the machine running it.

This software has dependencies to JavaScript and CSS libraries:

* [TableFilter](https://www.tablefilter.com)
* [Chart.js](https://www.chartjs.org/)
* [Leaflet](http://leafletjs.com/)
* [Leaflet.markercluster](https://github.com/Leaflet/Leaflet.markercluster)
* [Leaflet.MarkerCluster.LayerSupport](https://github.com/ghybs/Leaflet.MarkerCluster.LayerSupport)
* [Leaflet.Locate](https://github.com/domoritz/leaflet-locatecontrol)
* [Leaflet Control Geocoder](https://github.com/perliedman/leaflet-control-geocoder)
* [Leaflet-hash](https://github.com/mlevans/leaflet-hash)
* [Font Awesome](https://fontawesome.com/)
* [vanilla-i18n](https://thealphadollar.me/vanilla-i18n/)


## Explanations to the page

### Table
#### Sort

The columns are sortable. You can sort a column by clicking on the column headline in the table header at the top.

#### Column Description

* ID: This is the known ID of an OpenStreetMap object. It is linked to OpenStreetMap.org.
* Name: The column name contains the name of the object. If the object has no name, but an operator set, the operator is displayed. Otherwise it is empty.
* Tag Type: The tag type column displays which key contains the URL.
* URL: This is the URL tagged at the OpenStreetMap object at the time of querying the Overpass API.
* Status: The column status displays the HTTP status of the website or the error message, which is received when the website is unavailable.
* Analysis: This column shows the redirect URL, which was the last location after all redirects. It is only shown, when it is different to the URL in the column URL. The column also list other problems with the URL.
* Edit Links: This column has links to directly edit the corresponding OpenStreetMap object in the iD or JOSM editor. It also sets a default comment and a hashtag when using one of these editor links.

Clicking the edit links does not automatically change the OSM object in any editor. It is your responsibility to check the website and the OSM object and to do the needful change if any.

## How to use this software

### Install

Starting with Release 1.2.0 you can download the binaries for different OS and architectures. Only the Linux 64 Bit version is regularly tested. The other combinations of OS and architecture are untested.

The latest release can be found at: https://codeberg.org/strubbl/osm-link-validator/releases

Download and extract the archive.

### Run osm-link-validator

You can run the program e.g. by starting it like this:

```
$ cd osm-link-validator_7.2.3_Linux_x86_64
$ ./osm-link-validator -rel 62428
```

If you do not want to use a relation ID, the other possibility is to give the area parameter, which shall be an Overpass area ID. You can find out more about this special ID if you issue the help command:

```
$ ./osm-link-validator -h
Usage of ./osm-link-validator:
  -area int
    	Overpass API area ID as documented in https://wiki.openstreetmap.org/wiki/Overpass_API/Language_Guide#Area_clauses_(%22area_filters%22). Usually this is the relation ID plus 3600000000 or the way ID plus 2400000000, e.g. Munich relation ID is 62428 and thus the search area ID is 3600062428.
    	If no search area ID is given, the default ID 3601438081, representing Altefähr, is used. (default 3601438081)
  -config string
    	path to your config.json file (default "config.json")
  -d	print debug output
  -dd
    	like parameter -d, but print more debug output
  -ddd
    	like parameter -dd, but print even more debug output
  -rel int
    	Relation ID of the OSM object. This parameter is preferred compared to -area, e.g. when both parameters are given.
  -v	print the program's version
  ```

In the description of the area parameter you can learn how to get an ID for the area you want to analyze.

Hint: Do not select a very large area. Otherwise you might hit the [quotas of Overpass](https://dev.overpass-api.de/overpass-doc/en/preface/commons.html#quotas). E.g., for Munich the resulting Overpass JSON file is about 25 MB. The analysis of this data needs approx. one hour.


### Get the result

After running the osm-link-validator, you get the `output/` folder. The main HTML pages are created:

```
$ ls -lh output/*62428.html output/*62428.json
-rw-r--r-- 1 strubbl strubbl 6.2K Feb 11 07:13 output/about-3600062428.html
-rw-r--r-- 1 strubbl strubbl  92K Feb 11 07:13 output/chart-3600062428.html
-rw-r--r-- 1 strubbl strubbl 547K Feb 11 07:13 output/map-3600062428.html
-rw-r--r-- 1 strubbl strubbl 571K Feb 11 07:13 output/pois-no-url-3600062428.html
-rw-r--r-- 1 strubbl strubbl 250K Feb 11 07:13 output/statistics-3600062428.json
-rw-r--r-- 1 strubbl strubbl 534K Feb 11 07:13 output/table-3600062428.html
```

The resulting HTML page with the long table of findings is the file `table-3600374275.html` in the subfolder `output/`, which you can view with a browser, e.g. Firefox. The `statistics-3600374275.json` collects statistics about the daily status codes of all faulty URLs. They are visualized in the chart page. The map HTML page visualizes the analysis results on a map.


## References

### General

[Project home](https://codeberg.org/strubbl/osm-link-validator)

[Wiki page about the Area IDs](https://wiki.openstreetmap.org/wiki/Overpass_API/Language_Guide#Area_clauses_(%22area_filters%22))

[Nominatim search](https://nominatim.openstreetmap.org) to find your desired relation ID

### Evaluations of the Hashtag #strubblOLV

[Changesets with #strubblOLV](http://resultmaps.neis-one.org/osm-changesets?comment=strubblOLV) on Pascals' Resultmaps

[OSMCha Filter for #strubblOLV](https://osmcha.org/?aoi=eb084527-ee3e-4ffa-9c8e-6de368d5a155) (as [RSS Feed](https://osmcha.org/api/v1/aoi/eb084527-ee3e-4ffa-9c8e-6de368d5a155/changesets/feed/)) since 2022-09-10

[OhsomeNow Dashboard for #strubblOLV](https://stats.now.ohsome.org/dashboard#hashtag=strubblolv&start=2022-08-17T22:00:00Z&interval=P1M&countries=&topics=)


### Used OpenStreetMap Keys

#### Website Tags for detecting faulty URLs

* [contact:website](https://wiki.openstreetmap.org/wiki/Key:contact:website)
* [website](https://wiki.openstreetmap.org/wiki/Key:website)
* [url](https://wiki.openstreetmap.org/wiki/Key:url)
* [image](https://wiki.openstreetmap.org/wiki/Key:image)
* [heritage:website](https://wiki.openstreetmap.org/wiki/Key:heritage:website)
* [brand:website](https://wiki.openstreetmap.org/wiki/Key:brand:website)
* [operator:website](https://wiki.openstreetmap.org/wiki/Key:operator:website)
* [memorial:website](https://wiki.openstreetmap.org/wiki/Key:memorial:website)
* [network:website](https://wiki.openstreetmap.org/wiki/Key:network:website)
* [payment:website](https://wiki.openstreetmap.org/wiki/Key:payment:website)
* [website:menu](https://wiki.openstreetmap.org/wiki/Key:website:menu)
* [website:stock](https://wiki.openstreetmap.org/wiki/Key:website:stock)
* [website:map](https://wiki.openstreetmap.org/wiki/Key:website:map)
* [website:booking](https://wiki.openstreetmap.org/wiki/Key:website:booking)
* [source:website](https://wiki.openstreetmap.org/wiki/Key:source:website)
* [website:source](https://wiki.openstreetmap.org/wiki/Key:website:source)
* [xmas:url](https://wiki.openstreetmap.org/wiki/Key:xmas:url)

#### Feature Tags for searching for POIs without URL

* [amenity=kindergarten](https://wiki.openstreetmap.org/wiki/Tag:amenity%3Dkindergarten)
* [amenity=restaurant](https://wiki.openstreetmap.org/wiki/Tag:amenity%3Drestaurant)
* [amenity=school](https://wiki.openstreetmap.org/wiki/Tag:amenity%3Dschool)
* [leisure=marina](https://wiki.openstreetmap.org/wiki/Key:leisure)
* [tourism=hotel](https://wiki.openstreetmap.org/wiki/Key:tourism)
* [amenity=cafe](https://wiki.openstreetmap.org/wiki/Tag:amenity%3Dcafe)

Having no URL means not having any of the tags:

* contact:website
* contact:facebook
* contact:instagram
* contact:tiktok
* website
* url


### Related Projects

* [Osmose](https://osmose.openstreetmap.fr/de/map/#zoom=13&lat=48.1386&lon=11.6055&item=3093&level=1%2C2%2C3) has a URL Check
* Keepright.at had website checks, see https://sourceforge.net/p/keepright/code/HEAD/tree/checks/0410_website.php, but they were deactivated with commit https://sourceforge.net/p/keepright/code/834/

### Thanks to

#### Color selection

* the color palette provider https://flatuicolors.com/palette/de

#### Translators

* @Dirk
* @gitcookie
* @lejun
* @mondstern
* @SomeTr
* @Vistaus
