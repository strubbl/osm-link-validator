package main

import (
	"log"
	"os"
)

const defaultCacheDir = "cache"
const defaultOsmUserName = "Strubbl"
const defaultOutputDir = "output"
const defaultWebOutputSubdir = "/"

func readWriteConfigFile(givenConfigPath string, olvConfig *OlvConfiguration) error {
	if _, err := os.Stat(givenConfigPath); os.IsNotExist(err) {
		if *d {
			// in case config file does not exist, we cannot prefill the data from json
			// thus we create a default config
			log.Printf("readWriteConfigFile: file does not exist %s\n", givenConfigPath)
		}
		olvConfig.OsmUserName = defaultOsmUserName
		olvConfig.CachePath = defaultCacheDir
		olvConfig.OutputPath = defaultOutputDir
		err2 := writeNewJSON(*olvConfig, givenConfigPath)
		if err2 != nil {
			if *d {
				log.Println("readWriteConfigFile: error during writing the fresh config to disk:", err2)
			}
			return err2
		}
	}
	// assuming at this point the config exists, at least our just created default config
	err := readJSON(olvConfig, givenConfigPath)
	if err != nil {
		return err
	}
	configChanged := validateConfig(olvConfig)
	if configChanged {
		err = writeNewJSON(*olvConfig, givenConfigPath)
		if err != nil {
			if *d {
				log.Println("readWriteConfigFile: error during writing the validated config back to disk:", err)
			}
			return err
		}
	}
	return nil
}

func validateConfig(oc *OlvConfiguration) bool {
	configChanged := false
	if oc.CachePath == "" {
		oc.CachePath = defaultCacheDir
		configChanged = true
	}
	if oc.OsmUserName == "" {
		oc.OsmUserName = defaultOsmUserName
		configChanged = true
	}
	if oc.OutputPath == "" {
		oc.OutputPath = defaultOutputDir
		configChanged = true
	}
	if oc.WebOutputSubdir == "" {
		oc.WebOutputSubdir = defaultWebOutputSubdir
		configChanged = true
	}
	return configChanged
}
